#!/usr/bin/env python3

from setuptools import (
    find_packages,
    setup,
)

setup(
    name="b64json",
    version="1.0",
    description=("Crappy GTK editor for json encoded in base64"),
    long_description=("Crappy GTK editor for json encoded in base64"),
    url="https://github.com/jflesch/b64json",
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: X11 Applications :: GTK",
        "Environment :: X11 Applications :: Gnome",
        "Intended Audience :: End Users/Desktop",
        ("License :: OSI Approved ::"
         " GNU General Public License v3 or later (GPLv3+)"),
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Topic :: Multimedia :: Graphics :: Capture :: Scanners",
    ],
    license="GPLv3+",
    author="Jerome Flesch",
    author_email="jflesch@openpaper.work",
    packages=find_packages('src'),
    package_dir={'': 'src'},
    include_package_data=True,
    entry_points={
        'gui_scripts': [
            'b64json = b64json.main:main',
        ]
    },
    zip_safe=True,
    install_requires=[]
)
