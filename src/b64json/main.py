import base64
import json
import logging
import os
import pkg_resources
import sys

import gi
gi.require_version('Gtk', '3.0')

from gi.repository import GLib  # noqa: E402
from gi.repository import Gtk  # noqa: E402


logger = logging.getLogger(__name__)


def _get_resource_path(filename, pkg="b64json"):
    """
    Gets the absolute location of a datafile packaged.
    This function throws if the file is not found, but the error depends on the
    way the package was installed.

    Arguments:
        filename -- the relative filename of the file to load.

    Returns:
        the full path of the file.

    Throws:
        Exception -- if the file is not found.
    """
    if getattr(sys, 'frozen', False):
        path = os.path.join(sys._MEIPASS, "data", os.path.basename(filename))
        if os.path.exists(path):
            return path

    path = pkg_resources.resource_filename(pkg, filename)

    if not os.access(path, os.R_OK):
        raise FileNotFoundError(  # NOQA (Python 3.x only)
            "Can't find resource file '%s'. Aborting" % filename
        )

    logger.debug("For filename '%s' got file '%s'", filename, path)
    return path


def load_uifile(filename):
    """
    Load a .glade file and return the corresponding widget tree

    Arguments:
        filename -- glade filename to load.

    Returns:
        GTK Widget tree

    Throws:
        Exception -- If the file cannot be found
    """
    widget_tree = Gtk.Builder()
    ui_file = _get_resource_path(filename)
    widget_tree.add_from_file(ui_file)
    return widget_tree


class B64Json(object):
    def __init__(self):
        formatter = logging.Formatter(
            '%(levelname)-6s %(asctime)-15s %(name)-10s %(message)s'
        )
        sh = logging.StreamHandler()
        sh.setFormatter(formatter)
        logger = logging.getLogger()
        logger.addHandler(sh)

        self.widget_tree = load_uifile("b64json.glade")

        self.signal = True

    @staticmethod
    def get_text(buf):
        start = buf.get_iter_at_offset(0)
        end = buf.get_iter_at_offset(-1)
        return buf.get_text(start, end, False)

    def _base_updated(self, basebuffer):
        if not self.signal:
            return
        self.signal = False
        try:
            print("Base updated")
            txt = self.get_text(basebuffer)
            js = json.loads(txt)
            txt = json.dumps(js, indent=4)
            self.widget_tree.get_object("jsonBuffer").set_text(txt)

            txt = json.loads(txt)
            txt = txt['traces']
            txt = base64.decodebytes(txt.encode()).decode()
            self.widget_tree.get_object("tracesBuffer").set_text(txt)
        finally:
            self.signal = True

    def _json_updated(self, jsonbuffer):
        if not self.signal:
            return
        self.signal = False
        try:
            print("Json updated")
            txt = self.get_text(jsonbuffer)
            self.widget_tree.get_object("baseBuffer").set_text(txt)
            txt = json.loads(txt)
            txt = txt['traces']
            txt = base64.decodebytes(txt.encode()).decode()
            self.widget_tree.get_object("tracesBuffer").set_text(txt)
        finally:
            self.signal = True

    def _traces_updated(self, tracesBuffer):
        if not self.signal:
            return
        self.signal = False
        try:
            print("Traces updated")
            txt = self.get_text(tracesBuffer)
            txt = base64.encodebytes(txt.encode()).decode()

            jsonbuffer = self.widget_tree.get_object("jsonBuffer")
            js = self.get_text(jsonbuffer)
            js = json.loads(js)
            js['traces'] = txt
            js = json.dumps(js, indent=4)

            self.widget_tree.get_object("jsonBuffer").set_text(js)
            self.widget_tree.get_object("baseBuffer").set_text(js)
        finally:
            self.signal = True

    def main(self):
        main_loop = GLib.MainLoop()
        application = Gtk.Application()

        mainwin = self.widget_tree.get_object("mainwin")
        mainwin.show_all()

        mainwin.connect("destroy", lambda w: main_loop.quit())

        self.widget_tree.get_object("baseBuffer").connect(
            "changed", self._base_updated
        )
        self.widget_tree.get_object("jsonBuffer").connect(
            "changed", self._json_updated
        )
        self.widget_tree.get_object("tracesBuffer").connect(
            "changed", self._traces_updated
        )

        application.register()
        application.add_window(mainwin)
        main_loop.run()


def main():
    B64Json().main()


if __name__ == "__main__":
    main()
